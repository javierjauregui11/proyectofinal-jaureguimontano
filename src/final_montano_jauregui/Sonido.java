package final_montano_jauregui;

import java.io.File;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;

public class Sonido {
	
	Clip clip;
	
	public Sonido(){
		
	}
	
	private void playSoundLoop(String path){
		try {
		       AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(path).getAbsoluteFile());
		       clip = AudioSystem.getClip();
		       clip.open(audioInputStream);
		       clip.start();
		       clip.loop(Clip.LOOP_CONTINUOUSLY);
		   } catch(Exception ex) {
			   System.out.println("Error en la reproduccion del sonido.");
		       ex.printStackTrace();
		   }
	}
	private void stop()
	{
		clip.stop();
	}
	
	private void playSound(String path){
		try {
				
		       AudioInputStream audioInputStream = AudioSystem.getAudioInputStream(new File(path).getAbsoluteFile());
		       Clip clip2 = AudioSystem.getClip();
		       clip2.open(audioInputStream);
		       clip2.start();
		   } catch(Exception ex) {
			   System.out.println("Error en la reproduccion del sonido.");
		       ex.printStackTrace();
		   }
	}
	
	public void playBackgroundSound()
	{
		int i =(int)Math.round(Math.random()*3);
		System.out.println("tag"+i);
		switch(i) {
		case 0:
		case 1:
			playSoundLoop("sonidos/ACDC.wav");
			break;
		case 2:
			playSoundLoop("sonidos/Eye Of The Tiger.wav");
			break;
		default:
			playSoundLoop("sonidos/Rolling in the Deep.wav");
		}
		
	}
	public void stopBackgroundSound()
	{
		stop();
	}
	
	public void playExplosionDeJefeYJugadorSound()
	{
		
			playSound("sonidos/explosion (4).wav");
		
	}
	
	public void playExplosionSound()
	{
		int i =(int)Math.round(Math.random()*4);
		System.out.println("num"+i);
		switch(i) {
		case 1:
			playSound("sonidos/explosion (1).wav");
			break;
		case 2:
			playSound("sonidos/explosion (2).wav");
			break;
		case 3:
			playSound("sonidos/explosion (3).wav");
			break;
		default:
			playSound("sonidos/explosion (5).wav");
		}
		
	}
	
	public void playDisparoSound()
	{

			playSound("sonidos/disparo.wav");


	}
	public void playDisparojefeSound()
	{
			
			playSound("sonidos/disparojefe.wav");
			

	}
	
	public void playBalaEnJefeSound()
	{
		playSound("sonidos/BalasyJefe.wav");
	}
	public void playEntradaJefeSound()
	{
		playSound("sonidos/explosion (6).wav");
	}
	public void playMusicaJefeFinal() {
		playSoundLoop("sonidos/piratas del caribe.wav");
		
	}
	public void playmuerteJugadorSound()
	{
		playSound("sonidos/GRITO.wav");
		
	}
	public void playmuerteJugadoSound() {
		playSoundLoop("sonidos/jugadorMuerto.wav");
	}
	public void playGanoSound() {
		playSoundLoop("sonidos/Gane.wav");
	}
	
	

}
