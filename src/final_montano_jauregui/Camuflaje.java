package final_montano_jauregui;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
public class Camuflaje extends Rectangle {
	protected static double Ancho=10,Alto=10;
	double x,y;
	public Camuflaje(double x,double y,double xMax,double yMax,Color color) {
		super(Ancho,Alto,color);
		this.x=Math.random()*(xMax-Ancho);
		this.y=Math.random()*(yMax-Alto);
		setX(x+this.x);
		setY(y+this.y);
	}
	public Camuflaje(double x,double y,double xMax,double yMax,double Ancho,double Alto,Color color) {
		super(Ancho,Alto,color);
		this.x=Math.random()*(xMax-Ancho);
		this.y=Math.random()*(yMax-Alto);
		setX(x+this.x);
		setY(y+this.y);
	}
	
	public void actualizar(double x, double y) {
		setX(x+this.x);
		setY(y+this.y);
	}

}
