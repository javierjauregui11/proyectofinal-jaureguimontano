package final_montano_jauregui;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Pane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class fxController {

	@FXML
	private Pane paneCancha;
	static final double anchoCanchaa = 600;
	static final double altoCanchaa=850;

	private Jugador player;
	private Enemigos[] enemigos = new Enemigos[7];
	private Bala[] balas = new BalasJugador[10];
	private JefeFinal jefe;
	int balasDisparadas = 0;
	private Rectangulo r1;
	private Rectangulo r2;
	private Rectangulo r3;
	private Rectangulo r4,r5;
	static boolean murioElJugador;
	long instanteUltimoDisparo;
	Camuflaje[] camuflaje=new Camuflaje[50]; 
	@FXML
	private Button botonComenzar;
	static Sonido sonido;

	public fxController() {
		System.out.println("Constructor fxController");
		sonido=new Sonido();
	}

	@FXML
	public void initialize() {
		System.out.println("Inicializador fxController");
		// Limitar el espacio de la Cancha, para que la figura
		// no se dibuje fuera
		instanteUltimoDisparo = 0;
		final double anchoCancha = anchoCanchaa ;
		final double altoCancha = altoCanchaa;
		sonido.playBackgroundSound();
		Rectangle clip = new Rectangle(0, 0, 0, 0);
		jefe=new JefeFinal(anchoCancha,altoCancha,balas,player);
		//objetos
		player = new Jugador(anchoCancha, altoCancha,jefe);
		for(int i = 0 ;i<camuflaje.length;i++) {
			camuflaje[i]=new Camuflaje(0,0,anchoCanchaa,altoCanchaa,getColoreSENARIO());
		}
		
		
		for (int i = 0; i < enemigos.length; i++) {
			Color color= getColor();
			enemigos[i] = new Enemigos(anchoCancha,altoCancha/*, (int) carriles * i*/,0-Enemigos.Alto-Cannon.Alto ,color, balas,i,player);
			enemigos[i].iniciado();
		}
		for (int i = 0; i < balas.length; i++) {
			balas[i] = new BalasJugador((int) player.getX() + player.Ancho/ 2, (int) player.getY(), enemigos,jefe);
		}
		r5=new Rectangulo(0,0,anchoCancha+100,altoCancha+100,Color.LIGHTGREY);
		r1=new Rectangulo(anchoCancha-player.barra.Ancho/2,30,player.barra.Ancho,altoCancha,Color.RED);
		r2=new Rectangulo(anchoCancha-player.barra.Ancho-80,0,anchoCancha-player.barra.Ancho,altoCancha+30,Color.BROWN);
		r3=new Rectangulo(anchoCancha-player.barra.Ancho-80,altoCancha,anchoCancha-player.barra.Ancho,altoCancha+30,Color.BROWN);
		r4=new Rectangulo(0,altoCancha+50,anchoCancha+100,altoCancha+50,Color.LIGHTSLATEGREY);

		
		//evento teclado
		clip.widthProperty().bind(paneCancha.widthProperty());
		clip.heightProperty().bind(paneCancha.heightProperty());
		paneCancha.setClip(clip);

		paneCancha.getParent().setOnKeyPressed((event) -> {

			if (event.getCode().equals(KeyCode.LEFT) && player.canon.getX()-(Enemigos.Ancho/2)>0 && !BarraVida.murioElJugador&&jefe.vidaDelJefe>0) {
				player.mover(-15);
				for (int i = 0; i < balas.length; i++) {
					balas[i].actualizar(player.getX());
				}
			}
			if (event.getCode().equals(KeyCode.RIGHT)&& player.canon.getX()+(player.canon.Ancho/2)-(Enemigos.Ancho/2)<anchoCancha-150 && !BarraVida.murioElJugador&&jefe.vidaDelJefe>0) {
				player.mover(15);
				for (int i = 0; i < balas.length; i++) {
					balas[i].actualizar(player.getX());
				}
			}
			if (event.getCode().equals(KeyCode.UP)&& !BarraVida.murioElJugador&&jefe.vidaDelJefe>0) {
				long instanteActual = System.currentTimeMillis();
				if (instanteActual - instanteUltimoDisparo > 200) {
					sonido.playDisparoSound();
					System.err.println("Disparando bala #" + (balasDisparadas % balas.length));
					balas[balasDisparadas % balas.length].disparar();
					balasDisparadas++;
					instanteUltimoDisparo = instanteActual;
				}
				
			}
			
		});

		
		//pintar objetos
		paneCancha.getChildren().add(r5);
		paneCancha.getChildren().addAll(camuflaje);
		paneCancha.getChildren().addAll(enemigos);
		for(int i = 0 ; i < enemigos.length;i++) {
			paneCancha.getChildren().addAll(enemigos[i].explosion);

			paneCancha.getChildren().addAll(enemigos[i].camuflaje);
			paneCancha.getChildren().add(enemigos[i].canon);
			paneCancha.getChildren().addAll(enemigos[i].escotilla);
			
		}
	
		paneCancha.getChildren().add(r2);
		paneCancha.getChildren().add(r1);
		paneCancha.getChildren().add(r3);
		paneCancha.getChildren().add(player.barra);
	    
		
		paneCancha.getChildren().addAll(jefe.conjuntoJefe);
		for(int i = 0 ; i < jefe.conjuntoJefe.length;i++) {
			paneCancha.getChildren().addAll(jefe.conjuntoJefe[i].explosion);
			paneCancha.getChildren().addAll(jefe.conjuntoJefe[i].camuflaje);
			paneCancha.getChildren().addAll(jefe.conjuntoJefe[i].bala);
			paneCancha.getChildren().add(jefe.conjuntoJefe[i].canon);
			paneCancha.getChildren().add(jefe.conjuntoJefe[i].escotilla);
			
		}
		paneCancha.getChildren().addAll(balas);
		paneCancha.getChildren().addAll(player.explosion);
		paneCancha.getChildren().addAll(player.explosion2);
		paneCancha.getChildren().add(player);
		paneCancha.getChildren().addAll(player.camuflaje);
		paneCancha.getChildren().addAll(player.escotilla);
		paneCancha.getChildren().add(player.canon);
		
		paneCancha.getChildren().add(r4);
		
		
		
		//Actualizarlos
		AnimationTimerJuego miTimer = new AnimationTimerJuego(enemigos, balas,player,jefe,r4);
		miTimer.start();

	}

	private Color getColoreSENARIO() {
		int i = (int)Math.round( Math.random() * 4);
		switch (i) {
		case 0:
			
			return Color.DARKSLATEGRAY;
		case 1:
			
			return Color.LIGHTSLATEGREY;
		case 2:
		
			return Color.GRAY;
		default:
			
			return Color.STEELBLUE;
		}
	}

	@FXML
	public void botonSalirClick() {
		System.out.println("Se presion� el boton salir");
		System.exit(0);
	}

	@FXML
	public void botonComenzarClick() {
		System.out.println("Se presion� el boton comenzar");

	}

	public static Color getColor() {
		int i = (int)Math.round( Math.random() * 4);
		switch (i) {
		case 0:
			
			return Color.BLACK;
		case 1:
			
			return Color.YELLOW;
		case 2:
		
			return Color.CADETBLUE;
		default:
			
			return Color.DARKBLUE;
		}
	}

	public static void pararLaMusica() {
		sonido.stopBackgroundSound();
		sonido.playMusicaJefeFinal();
		
	}

}
