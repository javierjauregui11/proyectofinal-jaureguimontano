package final_montano_jauregui;

import javafx.animation.AnimationTimer;
import javafx.scene.paint.Color;

public class AnimationTimerJuego extends AnimationTimer {
	Enemigos[] enemigos;
	Bala[] balas;
	Jugador player;
	boolean jugadorMuerto = true;
	int enemigosMuertos;
	JefeFinal jefe;
	int[] inicio ;
	Sonido[] sonido;
	Rectangulo r4;
	boolean n;
	public AnimationTimerJuego(Enemigos[] enemigos, Bala[] balas, Jugador player, JefeFinal jefe,Rectangulo r4) {
		this.enemigos = enemigos;
		this.balas = balas;
		this.player = player;
		this.jefe = jefe;
		enemigosMuertos = 0;
		inicio= new int[jefe.conjuntoJefe.length];
		sonido=new Sonido[jefe.conjuntoJefe.length];
		for (int i = 0; i < inicio.length; i++) {
			inicio[i] = 0;
			sonido[i]=new Sonido();
		}
		this.r4=r4;
		n=true;
	}

	@Override
	public void handle(long arg0) {
		enemigosMuertos = 0;
		if (BarraVida.murioElJugador && jugadorMuerto) {
			for (int i = 0; i < player.explosion.length; i++) {
				player.explosion[i].setX(player.getX() + (player.Ancho - player.explosion[i].Ancho) / 2);
				player.explosion[i].setY(player.getY() + (player.Alto - player.explosion[i].Alto) / 2);
			}
			player.mover(-1000);
			for (int i = 0; i < balas.length; i++) {
				balas[i].actualizar(player.getX());
			}
			jugadorMuerto = false;
		}
		if (BarraVida.murioElJugador) {

			for (int i = 0; i < player.explosion.length; i++) {
				player.explosion[i].hacerExplocion();
			}
		}

		for (int i = 0; i < enemigos.length; i++) {
			enemigos[i].actualizar();
			enemigos[i].iniciado();
			if (enemigos[i].iniciado) {
				enemigosMuertos = 1;
				continue;
			}
		}
		if (enemigosMuertos == 0 && jefe.vidaDelJefe > 0) {

			jefe.iniciarJefe();
			for (int j = 0; j < jefe.conjuntoJefe.length; j++) {
				jefe.conjuntoJefe[j].explosion();
			}
		} else if (!(jefe.vidaDelJefe > 0)) {
			for (int j = 0; j < jefe.conjuntoJefe.length; j++) {
				if (inicio[j] == 0) {
					sonido[j].playExplosionDeJefeYJugadorSound();
					for (int o = 0; o < jefe.conjuntoJefe[j].explosion.length; o++) {
						
						jefe.conjuntoJefe[j].explosion[o].actualizar(jefe.conjuntoJefe[j].getX(),
								jefe.conjuntoJefe[j].getY());
						inicio[j] = 1;
					}
				}
				jefe.conjuntoJefe[j].setY(0 - Enemigos.Alto * 2);
				for (int i = 0; i < jefe.conjuntoJefe.length; i++) {
					jefe.conjuntoJefe[i].setY(jefe.conjuntoJefe[i].getY() + 1);
					jefe.conjuntoJefe[i].canon.actualizarCanonDeEnemigo(jefe.conjuntoJefe[i].getX(),
							jefe.conjuntoJefe[i].getY());
					jefe.conjuntoJefe[i].escotilla.actualizar(jefe.conjuntoJefe[i].getX(), jefe.conjuntoJefe[i].getY());
					jefe.conjuntoJefe[i].bala.reposicionar(jefe.conjuntoJefe[i].getY());
					for (int l = 0; l < jefe.conjuntoJefe[i].camuflaje.length; l++) {
						jefe.conjuntoJefe[i].camuflaje[l].actualizar(jefe.conjuntoJefe[i].getX(),
								jefe.conjuntoJefe[i].getY());
					}
					for (int o = 0; o < jefe.conjuntoJefe[j].explosion.length; o++) {
						jefe.conjuntoJefe[j].explosion[o].hacerExplocion();
					}
				}
			}
			
		}
		if((jefe.vidaDelJefe<=0||BarraVida.murioElJugador)) {
			if(n&&BarraVida.murioElJugador) {
				
				r4.setY(0-r4.getHeight());
			}
			
			if(BarraVida.murioElJugador&&r4.getY()<0) {
				if(n) {
				fxController.sonido.stopBackgroundSound();
				sonido[0].playmuerteJugadorSound();
				fxController.sonido.playmuerteJugadoSound();
				n=false;}
				
				r4.setY(r4.getY()+1);	
			}else if(r4.getY()>0){
				if(n) {
					fxController.sonido.stopBackgroundSound();
					fxController.sonido.playGanoSound();
					n=false;}
				r4.setY(r4.getY()-3);
			}
		}

		for (int i = 0; i < balas.length; i++) {
			balas[i].actualizar();
		}
		player.actualizar();

	}

}
