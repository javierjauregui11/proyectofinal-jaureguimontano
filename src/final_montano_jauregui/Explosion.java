package final_montano_jauregui;

import javafx.scene.paint.Color;

public class Explosion extends Camuflaje {
	double x,y;
	public Explosion(double x ,double y ,double xMax, double yMax, Color color) {
		super(0, 0, xMax, yMax,5,5, color);
		super.x=(xMax-Ancho)/2;
		super.y=(yMax-Alto)/2;
		setX(x+(xMax-Ancho)/2);
		setY(y+(yMax-Alto)/2);
		this.x=(Math.random()*10+1)-5;
		this.y=-Math.random()*5+1;
	}
	public Explosion(double x ,double y ,double xMax, double yMax, Color color,int i) {
		super(0, 0, xMax, yMax,5,5, color);
		super.x=(xMax-Ancho)/2;
		super.y=(yMax-Alto)/2;
		setX(x+(xMax-Ancho)/2);
		setY(y+(yMax-Alto)/2);
		this.x=(Math.random()*10+1)-5;
		this.y=(Math.random()*10+1)-5;
	}
	
	public void hacerExplocion() {
		setX(getX()+this.x);
		setY(getY()+this.y);
	}

	public void reposicionar(double x2, double y2) {
		setX(x2+super.x);
		setY(y2+super.y);
	}

}
