package final_montano_jauregui;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Bala extends Rectangle{
	protected static final int Ancho=10;
	protected static final int Alto=15;
	protected double velocidad;
	protected boolean disparada;
	
	
	public Bala(int x,int y, double vel ) {
		super(Ancho,Alto,Color.RED);
		velocidad=-10*vel;
		setX(x-Ancho/2);
		setY(y);
		disparada=false;
	}
	
	public void actualizar(double x) {
		if(!disparada){
			setX(x+(Jugador.Ancho-Ancho)/2);
		}
	}
	public void disparar() {
		 disparada=true;
	}
	public void actualizar() {
		
	}
	public void rebote(double reboteX) {
		setX(getX()+reboteX);
	}
	

}
