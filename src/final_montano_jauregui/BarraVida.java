package final_montano_jauregui;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;


public class BarraVida extends Rectangle {
	public static final int Ancho=25;
	public static double altoCancha;
	static boolean murioElJugador;
	Sonido sonido;
	
	public BarraVida(double anchoCancha,double altoCancha) {
		super(Ancho,altoCancha,Color.YELLOWGREEN);
		this.altoCancha=altoCancha;
		setX(anchoCancha-Ancho/2);
		setY(30);
		murioElJugador=false;
		sonido=new Sonido();
	}
	
	public void quitarVida(double vida) {
		if((int)getY()+80>=(int)altoCancha+20&&!murioElJugador) {
			sonido.playExplosionDeJefeYJugadorSound();
			murioElJugador=true;
		}
		setY(getY()+vida);
		
	}
	

}
