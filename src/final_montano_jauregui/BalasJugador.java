package final_montano_jauregui;

public class BalasJugador extends Bala{
	Enemigos[] enemigos;
	JefeFinal jefe;
	Sonido sonido;
	double reboteX;
	boolean rebote;
	
	public BalasJugador(int x,int y,Enemigos[] enemigos,JefeFinal jefe) {
		super(x,y,1);
		this.enemigos=enemigos;
		this.jefe=jefe;
		sonido= new Sonido();
		rebote=false;
	}
	
	public void actualizar(double x) {
		super.actualizar(x);
	}
	public void actualizar() {
		
		if(disparada) {
			setY(getY()+velocidad);
			if(getY()<=0) {
				rebote=false;
				disparada=false;
				System.err.println("Resetando bala");
				setX(Jugador.xj+(Jugador.Ancho-Ancho)/2);
				setY(Jugador.yj);
			}else if(!rebote){
				for(int i = 0;i< enemigos.length;i++) {
					if((getX()> enemigos[i].getX()&&getX()< enemigos[i].getX()+enemigos[i].Ancho&& getY()<=enemigos[i].getY())
							||(getX()+Ancho> enemigos[i].getX()&& getX()+Ancho< enemigos[i].getX()+enemigos[i].Ancho&& getY()<=enemigos[i].getY())) {
						sonido.playExplosionSound();
						if(!rebote) {
							sonido.playBalaEnJefeSound();
							reboteX=Math.random()*6-3;
						enemigos[i].muerto();
						}
						rebote=true;
//						setX(Jugador.xj+(Jugador.Ancho-Ancho)/2);
//						setY(Jugador.yj);
						
					}
				}
				for(int i = 0;i<jefe.conjuntoJefe.length;i++) {
					if((getX()> jefe.conjuntoJefe[i].getX()&&getX()< jefe.conjuntoJefe[i].getX()+jefe.conjuntoJefe[i].Ancho&& getY()<=jefe.conjuntoJefe[i].getY())
							||(getX()+Ancho> jefe.conjuntoJefe[i].getX()&& getX()+Ancho<jefe.conjuntoJefe[i].getX()+jefe.conjuntoJefe[i].Ancho&& getY()<=jefe.conjuntoJefe[i].getY())) {
						
						if(!rebote) {
							sonido.playBalaEnJefeSound();
							reboteX=Math.random()*6-3;
							jefe.vidaDelJefe-=55;
							System.out.println(jefe.vidaDelJefe);
							jefe.miniExplosion(getX(), getY(), i);
						}else {
							rebote(reboteX);
						}
						rebote=true;
						//setX(Jugador.xj+(Jugador.Ancho-Ancho)/2);
						//setY(Jugador.yj);					
				}
			}
		}else {
			rebote(reboteX);
		}
	}
	}
	

}
