package final_montano_jauregui;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Cannon extends Rectangle{
	public static double Ancho=10,Alto=50;
	public Cannon(double xJugador, double yJugador) {
		super(Ancho,Alto,Color.DARKGREEN);
		setX(xJugador+Jugador.Ancho/2-Ancho/2);
		setY(yJugador+Jugador.Alto/2-Alto);
	}
	public Cannon(double xJugador, double yJugador,double ancho,double alto,Color color) {
		super(Ancho,Alto,color);
		setX(xJugador+ancho/2-this.Ancho/2);
		setY(yJugador+alto/2);
	}
	public void actualizar(double x , double y ) {
		setX(x+Jugador.Ancho/2-Ancho/2);
	}
	public void actualizarCanonDeEnemigo(double x , double y ) {
		setX(x+Enemigos.Ancho/2-Ancho/2);
		setY(y+Enemigos.Alto/2);
	}
	

}
