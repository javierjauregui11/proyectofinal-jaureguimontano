package final_montano_jauregui;

import javafx.scene.paint.Color;

public class JefeFinal {
	Enemigos[] conjuntoJefe = new Enemigos[((int) fxController.anchoCanchaa - 120) / Enemigos.Ancho];
	double y;
	Sonido sonido;
	int idsinBala;
	int j = 0;
	static boolean disparar;
	int balasMuertos;
	static int vidaDelJefe;

	public JefeFinal(double anchoCancha, double alturaCancha, Bala[] balas, Jugador jugador) {
		y = -(Enemigos.Alto + Cannon.Alto * 6);
		Color color = fxController.getColor();
		double deltay = 0;
		for (int i = 0; i < conjuntoJefe.length; i++) {
			if (i == 0) {
				deltay = y;
			} else if (i < conjuntoJefe.length / 2) {
				deltay = conjuntoJefe[i - 1].getY() + 20;
			} else if (i > conjuntoJefe.length / 2) {

				deltay = conjuntoJefe[i - 1].getY() - 20;
			}
			conjuntoJefe[i] = new Enemigos(anchoCancha, alturaCancha, Enemigos.Ancho * i, deltay, color, balas, i,
					jugador);

		}
		y=0;
		disparar = false;
		j = 0;
		balasMuertos = 0;
		vidaDelJefe=5000;
		sonido=new Sonido();
	}

	public void iniciarJefe() {
		if(y==0) {
			fxController.sonido.playEntradaJefeSound();
			y=1;
			fxController.pararLaMusica();

		}
	

		if (conjuntoJefe[0].getY() <= 10||BarraVida.murioElJugador) {
			for (int i = 0; i < conjuntoJefe.length; i++) {
				conjuntoJefe[i].setY(conjuntoJefe[i].getY() + 1);
				conjuntoJefe[i].canon.actualizarCanonDeEnemigo(conjuntoJefe[i].getX(), conjuntoJefe[i].getY());
				conjuntoJefe[i].escotilla.actualizar(conjuntoJefe[i].getX(), conjuntoJefe[i].getY());
				for (int j = 0; j < conjuntoJefe[i].camuflaje.length; j++) {
					conjuntoJefe[i].camuflaje[j].actualizar(conjuntoJefe[i].getX(), conjuntoJefe[i].getY());
						conjuntoJefe[i].bala.setY(conjuntoJefe[i].getY()+Enemigos.Alto/2);
				}

			}
		} else {

			if (j == 0) {
				idsinBala=(int)(Math.random()*conjuntoJefe.length-1)+1;
				for (int i = 0; i < conjuntoJefe.length; i++) {
					if(i==idsinBala) {
						sonido.playDisparojefeSound();
						conjuntoJefe[i].bala.disparada = false;
						conjuntoJefe[i-1].bala.disparada = false;
					}else {
						
						conjuntoJefe[i].bala.disparada = true;
					}
					conjuntoJefe[i].bala.reposicionar(conjuntoJefe[i].getY() + Enemigos.Alto / 2);

					j = 1;
				}
			}
			for (int i = 0; i < conjuntoJefe.length; i++) {
				conjuntoJefe[i].bala.actualizar(conjuntoJefe[i].getY() + Enemigos.Alto / 2);
			}
			for (int i = 0; i < conjuntoJefe.length; i++) {
				if (!conjuntoJefe[i].bala.disparada) {
					balasMuertos++;
				}
			}

			if (balasMuertos == conjuntoJefe.length) {
				j = 0;
				balasMuertos=0;
			} else {
				balasMuertos = 0;
			}

		}
	}

	public void miniExplosion(double x, double y, int id) {
		for (int i = 0; i < conjuntoJefe.length; i++) {
			if (id == i) {
				for (int j = 0; j < conjuntoJefe[i].explosion.length; j++) {
					conjuntoJefe[i].iniciarExplosion = true;
					conjuntoJefe[i].explosion[j].setX(x);
					conjuntoJefe[i].explosion[j].setY(y + Enemigos.Alto);
				}
			}
		}

	}

}
