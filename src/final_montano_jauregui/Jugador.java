package final_montano_jauregui;

import javafx.scene.paint.Color;

import javafx.scene.shape.Rectangle;

public class Jugador extends Rectangle {
	public Cannon canon;
	public static final int Ancho = 75;
	public static final int Alto = 40;
	private static int x;
	public static int xj, yj;
	private double anchoCancha;
	BarraVida barra;
	Escotilla escotilla;
	Camuflaje[] camuflaje = new Camuflaje[100];
	static Color color = Color.DARKGREEN;
	Explosion[] explosion = new Explosion[500];
	Explosion[] explosion2 = new Explosion[100];
	JefeFinal jefe;
	boolean hacerExplosion;

	public Jugador(double anchoCancha, double alturaCancha, JefeFinal jefe) {
		super(Ancho, Alto, color);
		barra = new BarraVida(anchoCancha, alturaCancha);
		this.anchoCancha = anchoCancha;
		setX(((anchoCancha - 105) / 2) - (Ancho / 2));
		x = (int) getX();
		setY(alturaCancha);
		yj = (int) getY();
		xj = (int) getX();
		canon = new Cannon(getX(), getY());
		escotilla = new Escotilla(getX(), getY(), Ancho, Alto, color);

		for (int i = 0; i < camuflaje.length; i++) {
			camuflaje[i] = new Camuflaje(getX(), getY(), Ancho, Alto, getColor());
		}
		for (int i = 0; i < explosion.length; i++) {
			explosion[i] = new Explosion(getX(), getY(), Ancho, Alto, getColorExplosion());
		}
		for (int i = 0; i < explosion2.length; i++) {
			explosion2[i] = new Explosion(getX(), getY(), Ancho, Alto, getColorExplosion());
		}
		this.jefe = jefe;
		hacerExplosion = false;

	}

	public void mover(double velocidad) {
		setX(x += velocidad);
		xj = (int) getX();
		canon.actualizar(getX(), getY());
		escotilla.actualizar(getX(), getY());

		for (int i = 0; i < camuflaje.length; i++) {
			camuflaje[i].actualizar(getX(), getY());

		}
		for (int i = 0; i < explosion.length; i++) {
			if (!BarraVida.murioElJugador) {
				explosion[i].actualizar(getX(), getY());
			}

		}
		if(!hacerExplosion) {
		for (int i = 0; i < explosion2.length; i++) {
			explosion2[i].actualizar(getX(), getY());

		}
		}
	}

	public void actualizar() {
		for (int i = 0; i < jefe.conjuntoJefe.length; i++) {
			if ((int) (jefe.conjuntoJefe[i].bala.getY() + Bala.Alto) >= (int) getY()
					&& ((int) jefe.conjuntoJefe[i].bala.getX() >= (int) getX()
							&& (int) jefe.conjuntoJefe[i].bala.getX() <= (int) (getX() + Ancho))
					|| (int) (jefe.conjuntoJefe[i].bala.getY() + Bala.Alto) >= (int) getY()
							&& ((int) (jefe.conjuntoJefe[i].bala.getX() + Bala.Ancho) >= (int) getX()
									&& (int) (jefe.conjuntoJefe[i].bala.getX() + Bala.Ancho) <= (int) (getX()
											+ Ancho))) {
				fxController.sonido.playBalaEnJefeSound();
				hacerExplosion = true;
				for (int j = 0; j < explosion2.length; j++) {
					explosion2[j].setX(jefe.conjuntoJefe[i].bala.getX());
					explosion2[j].setY(jefe.conjuntoJefe[i].bala.getY() + Bala.Alto);

				}

				barra.quitarVida(60);
				jefe.conjuntoJefe[i].bala.disparada = false;
				jefe.conjuntoJefe[i].bala.reposicionar(jefe.conjuntoJefe[i].getY() + Enemigos.Alto / 2);
			}
			if (hacerExplosion) {

				for (int j = 0; j < explosion2.length; j++) {
					explosion2[j].hacerExplocion();

				}
			}
		}
	}

	public static Color getColor() {
		int i = (int) Math.round(Math.random() * 7);
		switch (i) {
		case 0:
			return Color.GREY;
		case 1:
			return Color.DARKOLIVEGREEN;
		case 2:
			return Color.LIGHTSEAGREEN;
		case 3:
			return Color.BLUEVIOLET;
		case 4:
			return Color.LIGHTBLUE;
		default:
			return Color.YELLOWGREEN;
		}
	}

	public Color getColorExplosion() {
		int i = (int) Math.round(Math.random() * 4);
		switch (i) {
		case 0:

			return Color.YELLOW;
		case 1:

			return Color.RED;
		case 2:

			return Color.ORANGE;
		default:
			return Color.ORANGERED;
		}
	}

}
