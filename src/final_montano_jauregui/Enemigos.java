package final_montano_jauregui;

import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

public class Enemigos extends Rectangle {
	public static final int Ancho = 40;
	public static final int Alto = 75;
	private double y;
	private double velocidad;
	Bala[] balas;
	Jugador jugador;
	public boolean muerto;
	public boolean iniciado;
	double alturaCancha;
	double anchoCancha;
	static int iniciados;

	static int Maxenemigos = 100;
	int id;
	Camuflaje[] camuflaje = new Camuflaje[80];
	Escotilla escotilla;
	Explosion[] explosion = new Explosion[100];
	BalasJefe bala;
	Cannon canon;
	/* iniciarExplosion */
	boolean iniciarExplosion;
	long tiempoAnterior;
	long tiempoAhora;
	boolean enemigoPasa;
	static boolean finDeEnemigos;
	private double yExplosion;

	public Enemigos(double anchoCancha, double alturaCancha, double y, Color color, Bala[] balas, int id,
			Jugador jugador) {
		super(Ancho, Alto, color);
		this.y = y;
		velocidad = Math.random() * 3 + 1;
		setX(Math.random() * (anchoCancha - Ancho - 105));
		setY(this.y);
		this.balas = balas;
		this.jugador = jugador;
		muerto = false;
		this.alturaCancha = alturaCancha;
		this.anchoCancha = anchoCancha;
		if (id == 0) {
			iniciado = false;
		} else {
			iniciado = false;
		}
		this.iniciados = 0;
		this.id = id;
		escotilla = new Escotilla(getX(), getY(), Ancho, Alto, color);
		canon = new Cannon(getX(), getY(), Ancho, Alto, color);
		for (int i = 0; i < camuflaje.length; i++) {
			camuflaje[i] = new Camuflaje(getX(), getY(), Ancho, Alto, fxController.getColor());
		}
		for (int i = 0; i < explosion.length; i++) {
			explosion[i] = new Explosion(getX(), getY(), Ancho, Alto, getColorExplosion(), i);
		}
		iniciarExplosion = false;
		finDeEnemigos = false;
		yExplosion = 0;

	}

	public Enemigos(double anchoCancha, double alturaCancha, double x, double y, Color color, Bala[] balas, int id,
			Jugador jugador) {
		this(anchoCancha, alturaCancha, y, color, balas, id, jugador);
		setX(x);
		escotilla = new Escotilla(getX(), getY(), Ancho, Alto, color);
		canon = new Cannon(getX(), getY(), Ancho, Alto, color);
		for (int i = 0; i < camuflaje.length; i++) {
			camuflaje[i] = new Camuflaje(getX(), getY(), Ancho, Alto, fxController.getColor());
		}
		for (int i = 0; i < explosion.length; i++) {
			explosion[i] = new Explosion(getX(), getY(), Ancho, Alto, getColorExplosion(), i);
		}
		
			bala=new BalasJefe((int)getX()+Ancho/2,(int)getY()+Alto,this.jugador);
		

	}

	public void actualizar() {
		if (iniciado && !iniciarExplosion) {
			if (muerto || getY() >= alturaCancha) {
				if (getY() >= alturaCancha) {
					jugador.barra.quitarVida(55);
					enemigoPasa = true;
				} else {
					iniciarExplosion = true;
					yExplosion = getY();
					tiempoAnterior = System.currentTimeMillis();
				}
				y = 0 - Alto - 20;
				velocidad = Math.random() * 2 + 1;
				setX(Math.random() * (anchoCancha - Ancho - 105));
				setY(y);
				if (iniciados >= Maxenemigos) {
					System.out.println("iniciado=false");
					iniciado = false;
				} else {
					muerto = false;
				}
				if (iniciados < Maxenemigos) {
					iniciados++;
				}
				System.out.println(iniciados);
			} else if (!muerto) {
				setY(y += velocidad);
				for (int i = 0; i < explosion.length; i++) {
					explosion[i].actualizar(getX(), getY());
				}
			}
			for (int i = 0; i < camuflaje.length; i++) {
				camuflaje[i].actualizar(getX(), getY());
			}

			escotilla.actualizar(getX(), getY());
			canon.actualizarCanonDeEnemigo(getX(), getY());
		} else if (iniciarExplosion && !enemigoPasa && yExplosion > 0) {

			tiempoAhora = System.currentTimeMillis();
			if (tiempoAhora - tiempoAnterior <= 1000) {
				for (int i = 0; i < explosion.length; i++) {
					explosion[i].hacerExplocion();
				}
			} else {
				iniciarExplosion = false;
				for (int i = 0; i < explosion.length; i++) {
					explosion[i].reposicionar(getX(), getY());
				}
			}
			enemigoPasa = false;
		} else {
			for (int i = 0; i < explosion.length; i++) {
				explosion[i].actualizar(getX(), getY());
			}

		}
		enemigoPasa = false;
	}

	public void muerto() {
		muerto = true;
	}

	public void iniciado() {
		if (id <= iniciados && (!muerto || iniciados < Maxenemigos)) {
			iniciado = true;
		}
	}

	public Color getColorExplosion() {
		int i = (int) Math.round(Math.random() * 6);
		switch (i) {
		case 0:

			return Color.YELLOW;
		case 1:

			return Color.RED;
		case 2:

			return Color.ORANGE;
		case 3:
			return Color.YELLOW;
		case 4:
			return Color.GOLD;
		default:
			return Color.ORANGERED;
		}
	}

	public void explosion() {
		tiempoAnterior = System.currentTimeMillis();
		

			tiempoAhora = System.currentTimeMillis();
			if (tiempoAhora - tiempoAnterior <= 1000) {
				for (int i = 0; i < explosion.length; i++) {
					explosion[i].hacerExplocion();
				}
			} else {
				iniciarExplosion = false;
				for (int i = 0; i < explosion.length; i++) {
					explosion[i].reposicionar(getX(), getY());
				}
			}
		
			}

}
