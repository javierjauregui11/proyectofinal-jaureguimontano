package final_montano_jauregui;

import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;

public class Escotilla extends Circle{
	static double radio=15;
	private double x,y;
	public Escotilla(double x,double y , double xMax,double yMax,Color color) {
		super(radio,color);
		this.x=xMax/2;
		this.y=yMax/2;
		setCenterX(x+this.x);
		setCenterY(y+this.y);	
	}
	public void actualizar(double x,double y) {
		setCenterX(x+this.x);
		setCenterY(y+this.y);	
	}

}
